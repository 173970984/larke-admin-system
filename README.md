## larke-admin通用后台管理系统


### 项目介绍

*  `larke-admin` 是基于 `laravel8` 版本的后台快速开发框架，完全api接口化，适用于前后端分离的项目
*  基于 `JWT` 的用户登录态管理
*  权限判断基于 `php-casbin` 的 `RBAC` 授权
*  本系统库为 `composer` 安装后的完整版本
*  系统核心可以查看 [larke-admin](https://github.com/deatil/larke-admin)

### 环境要求

 - PHP >= 7.3.0
 - Laravel >= 8.0.0
 - Fileinfo PHP Extension


### 截图预览

<table>
    <tr>
        <td width="50%">
            <center>
                <img alt="login" src="https://images.gitee.com/uploads/images/2021/0304/000601_d7c837fb_299.png" />
            </center>
        </td>
        <td width="50%">
            <center>
                <img alt="index" src="https://images.gitee.com/uploads/images/2021/0304/000635_ae9ec28e_299.png" />
            </center>
        </td>
    </tr>
    <tr>
        <td width="50%">
            <center>
                <img alt="admin" src="https://images.gitee.com/uploads/images/2021/0304/000713_fa74236a_299.png" />
            </center>
        </td>
        <td width="50%">
            <center>
                <img alt="admin-access" src="https://images.gitee.com/uploads/images/2021/0304/000734_2042abf3_299.png" />
            </center>
        </td>
    </tr>
    <tr>
        <td width="50%">
            <center>
                <img alt="attach" src="https://images.gitee.com/uploads/images/2021/0304/000758_1577df5c_299.png" />
            </center>
        </td>
        <td width="50%">
            <center>
                <img alt="config" src="https://images.gitee.com/uploads/images/2021/0304/000815_ae9e94c2_299.png" />
            </center>
        </td>
    </tr>
    <tr>
        <td width="50%">
            <center>
                <img alt="menus" src="https://images.gitee.com/uploads/images/2021/0304/000841_34c69947_299.png" />
            </center>
        </td>
        <td width="50%">
            <center>
                <img alt="rule2" src="https://images.gitee.com/uploads/images/2021/0304/000901_5510a63d_299.png" />
            </center>
        </td>
    </tr>
</table>


### 服务端安装步骤

1. 首先确认连接数据库的配置没有问题

2. 然后运行下面的命令，推送配置文件

```php
php artisan vendor:publish --tag=larke-admin-config
```

运行完命令后，你可以找到 `config/larkeadmin.php`、`config/larkeauth.php` 及 `config/larkeauth-rbac-model.conf` 三个配置文件

3. 最后运行下面的命令安装完成系统

```php
php artisan larke-admin:install
```

4. 你可能第一次安装需要运行以下命令导入路由权限规则

```php
php artisan larke-admin:import-route
```

5. 后台登录账号：`admin` 及密码 `123456`


### 前端安装步骤

1. 运行下面的命令，推送配置文件及静态文件

```php
php artisan vendor:publish --tag=larke-admin-frontend-config
```

```php
php artisan vendor:publish --tag=larke-admin-frontend-assets
```

运行命令后，你可以在 `public/admin` 下发现前端文件

你也可以找到 `config/frontend.php` 配置文件

2. 你可能第一次安装需要运行以下命令更新前端适配菜单信息

```php
php artisan larke-admin-frontend:import-menus --force
```

3. 你可以替换前端文件为你自己的打包好的前端文件

菜单接口可根据包提供的接口更改为适合你的菜单字段

4. 前端编译文档可以查看文档 `public/admin/README.zh-CN.md`

在编译前你需要复制 `.env.development.larke` 重命名为 `.env.development`，复制 `vue.config.js.larke` 重命名为 `vue.config.js`

5. 当前编译环境

 - node v9.9.0
 
 - npm 6.14.8


### 扩展推荐

| 名称 | 描述 |
| --- | --- |
| [demo](https://github.com/deatil/larke-admin-demo) | 扩展示例 |
| [签名证书](https://github.com/deatil/larke-admin-signcert) | 生成RSA,EDDSA,ECDSA等非对称签名证书 |
| [日志查看器](https://github.com/deatil/larke-admin-logviewer) | laravel日志查看扩展 |

注：扩展目录默认为 `/extension` 目录


### 特别鸣谢

感谢以下的项目,排名不分先后

 - laravel/framework

 - lcobucci/jwt

 - casbin/casbin

 - composer/semver

 - phpseclib/phpseclib
 
 - PclZip


### 开源协议

*  `larke-admin` 遵循 `Apache2` 开源协议发布，在保留本系统版权的情况下提供个人及商业免费使用。 


### 版权

*  该系统所属版权归 deatil(https://github.com/deatil) 所有。
